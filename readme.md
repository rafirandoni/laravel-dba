
### Installation

Add to composer.json
```
    "require-dev": {
        ...
        "rafirandoni/dba": "*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/rafirandoni/laravel-dba.git"
        }
    ]
```

### Export Route

Run on command line

```
php artisan dba:export:route --path=<path>
```

| Key   | Description |
| ----- | -------------|
| path  | export file path |