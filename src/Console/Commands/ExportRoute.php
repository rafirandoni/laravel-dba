<?php

namespace DBA\Console\Commands;

use Illuminate\Console\Command;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportRoute extends Command
{
    protected $signature = 'dba:export:route
        {--path= : Where should we store exported file? (root=/storage)}
        {--except= : List of route name that should not get exported}
        ';
    protected $description = 'Export Route Lists';

    // Destination exported file
    protected $destination_path;

    // Exclude given route name from being exported
    protected $except = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Configuration
        if ($this->option('except')) {
            $this->except = array_map('trim', explode(',', $this->option('except')));
        } else {
            $this->except = [
                'login',
                'logout',
                'password.email',
                'password.update',
                'password.request',
                'password.reset',
            ];
        }

        // Start process
        $this->destination_path = storage_path(($this->option('path') ? $this->option('path') : 'export'));
        if ( ! is_dir($this->destination_path)) {
            @mkdir($this->destination_path, 0755, true);
        }

        $this->info('Start Exporting Route');
        $this->info('Collecting Route Info');
        $routes = $this->get_route_lists();

        $this->info('Generating Export File');
        $generate_excel = $this->generate_excel($routes);

        $this->info('Export Finished');
    }

    protected function get_route_lists()
    {
        \Artisan::call('route:list');
        $routes = \Artisan::output();
        $rows = explode("\n", $routes);
        $raw_keys = explode('|', $rows[1]);
        $keys = array_map('trim', $raw_keys);

        unset ($rows[1]);

        $data = [];
        foreach ($rows as $key => $row) {
            if (strpos($row, '-----') !== false) continue;

            $offset = 0;
            $item = [];
            for ($i=1; $i < count($keys); $i++) {
                if ($keys[$i] == '') continue;

                $keys[$i] = strtolower($keys[$i]);

                $row = str_replace('|', '', $row);
                $length = strlen($raw_keys[$i]);

                $value = trim(substr($row, $offset, $length));

                $method = [];
                if (strpos($value, 'GET') !== false
                    || strpos($value, 'POST') !== false
                    || strpos($value, 'HEAD') !== false
                    || strpos($value, 'OPTION') !== false
                    || strpos($value, 'DELETE') !== false
                    || strpos($value, 'PUT') !== false
                ) {
                    while (strlen($value) > 0) {
                        if (strpos($value, 'GET') !== false) {
                            $method[] = 'GET';
                            $value = str_replace('GET', '', $value);
                        }
                        if (strpos($value, 'POST') !== false) {
                            $method[] = 'POST';
                            $value = str_replace('POST', '', $value);
                        }
                        if (strpos($value, 'HEAD') !== false) {
                            $method[] = 'HEAD';
                            $value = str_replace('HEAD', '', $value);
                        }
                        if (strpos($value, 'OPTION') !== false) {
                            $method[] = 'OPTION';
                            $value = str_replace('OPTION', '', $value);
                        }
                        if (strpos($value, 'DELETE') !== false) {
                            $method[] = 'DELETE';
                            $value = str_replace('DELETE', '', $value);
                        }
                        if (strpos($value, 'PUT') !== false) {
                            $method[] = 'PUT';
                            $value = str_replace('PUT', '', $value);
                        }
                    }
                }

                // Skip if route name was on except lists
                // echo ($keys[$i]);
                if ($keys[$i] == 'name') {
                    if (in_array($value, $this->except)) {
                        $item = null;
                        break;
                    }
                }

                $item[$keys[$i]] = count($method)>0 ? json_encode($method) : $value;
                $offset += $length;
            }

            if ( ! is_null($item)) {
                $data[] = $item;
            }
        }

        return $data;
    }

    protected function generate_excel($data = [])
    {
        if (count($data) < 1) {
            return false;
        }

        $spreadsheet = new Spreadsheet;
        $spreadsheet->getProperties()->setCreator('DBA Library')
            ->setLastModifiedBy('Me')
            ->setTitle('Route Lists')
            ->setSubject('Route Lists')
            ->setDescription('Route lists')
            ->setKeywords('route')
            ->setCategory('Export');

        $sheet = $spreadsheet->getActiveSheet();

        $keys = array_keys($data[0]);

        for ($i=0; $i < count($keys); $i++) {
            $sheet->setCellValue(strtoupper(chr(97 + $i)).'1', $keys[$i]);
        }

        foreach ($data as $key => $row) {
            for ($i=0; $i < count($keys); $i++) {
                $column = strtoupper(chr(97 + $i)).($key + 2);
                $sheet->setCellValue($column, $row[$keys[$i]]);
            }
        }

        $filename = 'RouteLists.xlsx';

        $writer = new Xlsx($spreadsheet);
        $writer->save($this->destination_path.DIRECTORY_SEPARATOR.$filename);
    }
}