<?php

namespace DBA;

use Illuminate\Support\ServiceProvider;

class DBAServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (app()->runningInConsole()) {
            $this->commands([
                Console\Commands\ExportRoute::class,
            ]);
        }
    }

    public function register()
    {
        # code...
    }
}